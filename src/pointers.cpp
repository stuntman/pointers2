#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;


  cout<<"x = "<<x<<", y = "<<y<< endl << endl;;

	cout << "Variable a is of data type: pointer" << endl;
	cout << "The variable a is located at address: " << &a << endl;
	cout << "The value of the variable a is: " << a << endl << "\n\n";
	
	
	cout << "Variable b is of data type: pointer"<< endl;
	cout << "The variable b is located at address: " << &b << endl;
	cout << "The value of the variable b is: " << b << endl<< "\n\n";
	
	cout << "Variable c is of data type: Integer"<< endl;
	cout << "The variable c is located at address: " << &c << endl;
	cout << "The value of the variable c is: " << c << endl<< "\n\n";
	
	cout << "Variable d is of data type: Integer"<< endl;
	cout << "The variable d is located at address: " << &d << endl;
	cout << "The value of the variable d is: " << d << endl<< "\n\n";

	cout << "The variable x is located at address: " << &y << endl;
	cout << "The value of the variable x is: " << x << endl<< "\n\n";
	
	cout << "The variable y is located at address: " << a << endl;
	cout << "The value of the variable y is: " << y << endl;
	
	
}
